function d = standardize(d)
%STANDARDIZE remove mean and set standard deviation to one
%   DATA = STANDARDIZE(DATA) standardize DATA.

    d = d - mean(d);
    d = d ./ std(d);
end
