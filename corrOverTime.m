function overTime(mat, window, windowWidth)
    if nargin < 3
        windowWidth = 1;
    end

    filename = 'corrOverTime.gif';
    upperTime = floor((size(mat, 1) - window) / windowWidth);
    for i = 1:upperTime
        idxStart = (i - 1) * windowWidth + 1;
        idxEnd = idxStart + window - 1;
        if i == 1
            h = plotMap(pearson(mat(idxStart:idxEnd, :)));
        else
            h.ColorData = pearson(mat(idxStart:idxEnd, :));
        end

        drawnow()
        im = frame2im(getframe(gcf));
        [imind, cm] = rgb2ind(im, 256);

        if i == 1
            imwrite(imind, cm, filename, 'gif', 'LoopCount', inf);
        else
            imwrite(imind, cm, filename, 'gif', 'WriteMode', 'append');
        end
    end
end

function h = plotMap(map)
    h = heatmap(map);

    colormap default
    labels = repmat({""}, [length(map) 1]);

    h.GridVisible = 'off';
    h.XDisplayLabels = labels;
    h.YDisplayLabels = labels;
    h.ColorLimits = [-1 1];
end
