function sigma = covar(mat)
    mat = mat - mean(mat, 1);
    sigma = (mat' * mat) / size(mat, 1);
end
