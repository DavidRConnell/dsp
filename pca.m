function results = pca(mat)
    results = sortedEig(dsp.covar(mat));
end

function V = sortedEig(vals)
    [V, D] = eig(vals);
    [~, idx] = sort(abs(diag(D)), 'descend');
    V = V(:, idx);
end
